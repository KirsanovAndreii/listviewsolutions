package com.ankir33gmail.listviewsolution;

import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<User> listUsers;
    ListView listView;
    MyAdapterListView myAdapterListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listUsers = createList();
        listView = (ListView) findViewById(R.id.list_view);
        myAdapterListView = new MyAdapterListView(this, listUsers);
        listView.setAdapter(myAdapterListView);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("keyName", listUsers.get(position).name);
                intent.putExtra("keyEmail", listUsers.get(position).email);
                intent.putExtra("keyAdress", listUsers.get(position).adress);

                intent.putExtra("keyPhone", listUsers.get(position).phone);
                intent.putExtra("keyIconca", listUsers.get(position).ikonka);
                startActivity(intent);
            }
        });
    }


    ArrayList<User> createList() {
        ArrayList<User> rezult = new ArrayList<>();
        for (int i = 1; i < 15; i++) {
            User user = new User();
            user.name = "Имя " + i;
            user.email = i + "@gmail.com";
            user.ikonka = R.drawable.pinkhellokitty;
            user.adress = "Харьков" + i + i;
            user.phone = "050-123-45-67";
            rezult.add(user);
        }
        return rezult;
    }

    public void onClickAdd(View view) {
        LayoutInflater factory = LayoutInflater.from(this);
        final View edit_text_multi = factory.inflate(R.layout.edit_text_mult, null);

        final EditText nameInput = (EditText) edit_text_multi.findViewById(R.id.edit_text_name);
        final EditText adressInput = (EditText) edit_text_multi.findViewById(R.id.edit_text_adress);
        final EditText emailInput = (EditText) edit_text_multi.findViewById(R.id.edit_text_email);
        final EditText phoneInput = (EditText) edit_text_multi.findViewById(R.id.edit_text_phone);

        AlertDialog.Builder alertAdd = new AlertDialog.Builder(this);
        alertAdd.setTitle("ADD item");
        alertAdd.setView(edit_text_multi);
        alertAdd.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                User user = new User();
                user.name = "Имя " + nameInput.getText().toString();
                user.adress = "Харьков" + adressInput.getText().toString();
                user.email = emailInput.getText().toString();
                user.ikonka = R.drawable.pinkhellokitty;
                user.phone = phoneInput.getText().toString();
                listUsers.add(user);
                myAdapterListView.notifyDataSetChanged();
            }
        });

        alertAdd.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alertAdd.show();
    }
}
