package com.ankir33gmail.listviewsolution;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by AnKir on 06.05.2017.
 */

public class SecondActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String name = getIntent().getExtras().getString("keyName");
        TextView text1 = (TextView) findViewById(R.id.textView1);
        text1.setText(name);

        String email = getIntent().getExtras().getString("keyEmail");
        TextView text2 = (TextView) findViewById(R.id.textView2);
        text2.setText(email);

        String adress = getIntent().getExtras().getString("keyAdress");
        TextView text3 = (TextView) findViewById(R.id.textView3);
        text3.setText(adress);

        String phone = getIntent().getExtras().getString("keyPhone");
        TextView text4 = (TextView) findViewById(R.id.textView4);
        text4.setText(phone);

        int iconca = getIntent().getExtras().getInt("keyIconca");
        ImageView img = (ImageView) findViewById(R.id.img_view5);
        img.setImageResource(iconca);
    }
}
