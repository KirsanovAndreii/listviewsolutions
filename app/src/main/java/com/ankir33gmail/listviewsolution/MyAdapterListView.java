package com.ankir33gmail.listviewsolution;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by AnKir on 05.05.2017.
 */

public class MyAdapterListView extends ArrayAdapter<User> {

    public MyAdapterListView(@NonNull Context context, @LayoutRes ArrayList<User> values) {
        super(context, R.layout.list_item, values);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder();
            holder.textViewName = (TextView) rowView.findViewById(R.id.text_view_name);
            holder.textViewEmail = (TextView) rowView.findViewById(R.id.text_view_email);
            holder.imageViewIconca = (ImageView) rowView.findViewById(R.id.iconca);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.textViewName.setText(getItem(position).name);
        holder.textViewEmail.setText(getItem(position).email);
        holder.imageViewIconca.setImageResource(getItem(position).ikonka);

        return rowView;
    }

    class ViewHolder {
        public TextView textViewName;
        public TextView textViewEmail;
        public ImageView imageViewIconca;
    }


}
